/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  Dimensions,
  Image,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from 'react-native';

const styles = StyleSheet.create({
  textForm: {
    backgroundColor: '#95a5a6',
    padding: 20,
    margin:10,
    borderRadius: 10,
  },
  img: {
    backgroundColor: '#95a5a6',
    width: 70,
    height: 70,
    borderRadius: 100
  },
  textPersona: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  textTitulo: {
    color: 'white',
    fontSize: 30
  }, 
  textSubTitulo: {
    color: 'white',
    fontSize: 20
  },
  textColumn:{
    width: 150,
    color: 'black',
    fontSize: 20
  },
  textBioTitulo: {
    color: '#7f8c8d',
    fontSize: 20
  },
  textBioSubTitulo: {
    color: 'black',
    fontSize: 20
  },
  mt20:{
    marginTop: 20
  },
  viewColumns:{
    flexDirection: 'row'
  }
  

});


const App: () => React$Node = () => { 

  const response = 'Cola';

  return (
    <>
      <StatusBar barStyle="light-content" />
      <SafeAreaView style={{flex:1, backgroundColor: '#ecf0f1'}}>
        <ScrollView>
          <View style={styles.textForm}>
            <View style={styles.textPersona}>
              <View>
                <Text style={styles.textTitulo}>
                  Felipe Monti
                </Text>  
                <Text style={styles.textSubTitulo}>
                  35
                </Text>
              </View>
              <Image style={styles.img}
                source={{uri: 'https://picsum.photos/seed/picsum/200'}}
                resizeMode='contain'
              >
              </Image>
            </View>  

            <View style={styles.mt20}>
              <Text style={styles.textBioTitulo}>
                  Bio
              </Text> 
              <Text style={styles.textBioSubTitulo}>
                    Ingeniero Civil en Computación.
              </Text>
            </View>


            <View style={styles.mt20}>
              <Text style={styles.textBioTitulo}>
                  Descripción
              </Text> 
              <View style={styles.viewColumns}>
                <Text style={styles.textColumn}>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nisl tincidunt eget nullam non. Quis hendrerit dolor magna eget est lorem ipsum dolor sit. Volutpat odio facilisis mauris sit amet massa. Commodo odio aenean sed adipiscing diam donec adipiscing tristique. Mi eget mauris pharetra et. Non tellus orci ac auctor augue. Elit at imperdiet dui accumsan sit. Ornare arcu dui vivamus arcu felis. Egestas integer eget aliquet nibh praesent. In hac habitasse platea dictumst quisque sagittis purus. Pulvinar elementum integer enim neque volutpat ac.
                </Text>
                <Text style={styles.textColumn}>
                  Senectus et netus et malesuada. Nunc pulvinar sapien et ligula ullamcorper malesuada proin. Neque convallis a cras semper auctor. Libero id faucibus nisl tincidunt eget. Leo a diam sollicitudin tempor id. A lacus vestibulum sed arcu non odio euismod lacinia. In tellus integer feugiat scelerisque. Feugiat in fermentum posuere urna nec tincidunt praesent. Porttitor rhoncus dolor purus non enim praesent elementum facilisis. Nisi scelerisque eu ultrices vitae auctor eu augue ut lectus. Ipsum faucibus vitae aliquet nec ullamcorper sit amet risus. Et malesuada fames ac turpis egestas sed. 
                </Text>
              </View>
            </View>


          </View>

        </ScrollView>
        

      </SafeAreaView>
    </>
  );
};

export default App;
 